# Custom fork of spf13-vim


My custom fork of spf13-vim with custom settings.

## Quick install from cli: ##
sh <(curl https://bitbucket.org/cigolpl/spf13-vim/raw/3.0/bootstrap.sh -L)

For more details go to the link https://github.com/spf13/spf13-vim

It has to be working with node.js very well.

## Install fresh modules: ##
cd $HOME/to/spf13-vim/
git pull
vim +BundleInstall! +BundleClean +q